CREATE TABLE IF NOT EXISTS USER (
    ID BIGSERIAL PRIMARY KEY,
    NAME VARCHAR(200) NOT NULL,
    LOGIN VARCHAR(20)  UNIQUE NOT NULL,
    PASSWORD VARCHAR(20) NOT NULL,
    ACTIVE BOOLEAN,
    CREATED_AT TIMESTAMP NOT NULL,
    UPDATED_AT TIMESTAMP
);


CREATE TABLE IF NOT EXISTS ATHLETE (
    ID BIGSERIAL PRIMARY KEY,
    NAME VARCHAR(200) NOT NULL,
    NICKNAME VARCHAR(200) NOT NULL,
    GRADUATION VARCHAR(100) NOT NULL,
    GENDER VARCHAR(50) NOT NULL,
    WEIGHT DECIMAL(100,2) NOT NULL,
    HEIGHT DECIMAL(100,2) NOT NULL,
    PHONE VARCHAR(20) NOT NULL,
    ADDRESS VARCHAR(255) NOT NULL,
    EMERGENCY_CONTACT VARCHAR(255) NOT NULL,
    ACTIVE BOOLEAN,
    COACH BOOLEAN,
    BIRTHDATE TIMESTAMP NOT NULL,
    STARTED_AT TIMESTAMP NOT NULL,
    CREATED_AT TIMESTAMP NOT NULL,
    UPDATED_AT TIMESTAMP
);

CREATE TABLE IF NOT EXISTS EVENT (
    ID BIGSERIAL PRIMARY KEY,
    DESCRIPTION VARCHAR(200) NOT NULL,
    LOCATION VARCHAR(200) NOT NULL,
    ACTIVE BOOLEAN,
    DATE TIMESTAMP NOT NULL,
    CREATED_AT TIMESTAMP NOT NULL,
    UPDATED_AT TIMESTAMP
);


INSERT INTO USER (NAME, EMAIL, LOGIN, PASSWORD, ACTIVE, ADMIN, CREATED_AT) VALUES ('Administrador', 'email@emailcom',
                                                                                   'admin', 'admin', true, true, now());