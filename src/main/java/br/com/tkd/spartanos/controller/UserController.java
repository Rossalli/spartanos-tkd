package br.com.tkd.spartanos.controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.com.tkd.spartanos.model.dao.UserDAO;
import br.com.tkd.spartanos.model.entity.User;
import br.com.tkd.spartanos.util.MessagesUtil;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@SessionScoped
public class UserController {

    private final UserDAO dao = new UserDAO();

    @Getter
    @Setter
    private String login;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private User user;

    @Getter
    @Setter
    private User newUser = new User();

    @Getter
    @Setter
    private boolean logged;

    @PostConstruct
    public void postConstruct() {
        newUser = new User();
    }

    public String login() throws Exception {
        User user = dao.login(login, password);
        logged = user != null;
        this.user = user;
        if (!logged) {
            MessagesUtil.message("Usuário e/ou senha incorreto", FacesMessage.SEVERITY_ERROR);
            return "";
        }
        return "spartanos/home.xhtml?faces-redirect=true";
    }


    public void logout() throws Exception {
        logged = false;
        this.user = null;
        //TODO: fix redirect not working
    }

    public String register() {
        dao.saveUser(newUser);
        newUser = new User();
        return "index.xhtml?faces-redirect=true";
    }

    public boolean validateUsername() {
        User user = dao.getByLogin(newUser.getLogin());
        if (user != null) {
            MessagesUtil.message("Login indisponível", FacesMessage.SEVERITY_ERROR);
            return false;
        }
        return true;
    }

}
