package br.com.tkd.spartanos.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import br.com.tkd.spartanos.model.dao.AthleteDAO;
import br.com.tkd.spartanos.model.entity.Athlete;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
public class ClassController {

    private final AthleteDAO dao = new AthleteDAO();

    @Getter
    @Setter
    private Date date = new Date();

    @Getter
    @Setter
    private List<Athlete> athleteList = new ArrayList<>();

    @PostConstruct
    public void postConstruct() {
        date = new Date();
        updateList();
    }

    private void updateList() {
        athleteList = new ArrayList<>();
        athleteList = dao.getAll();
    }


}
