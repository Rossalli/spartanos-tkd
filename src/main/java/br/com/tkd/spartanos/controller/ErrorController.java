package br.com.tkd.spartanos.controller;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class ErrorController {

    public String returnHome() {
        return "spartanos/home.xhtml?faces-redirect=true";
    }
}
