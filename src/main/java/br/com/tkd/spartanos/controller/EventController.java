package br.com.tkd.spartanos.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;

import org.apache.commons.lang3.StringUtils;

import br.com.tkd.spartanos.model.dao.EventDAO;
import br.com.tkd.spartanos.model.entity.Event;
import br.com.tkd.spartanos.util.MessagesUtil;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
public class EventController {

    private final EventDAO dao = new EventDAO();

    @Getter
    @Setter
    private List<Event> eventList = new ArrayList<>();

    @Getter
    @Setter
    private Event selectedEvent = new Event();

    @Getter
    @Setter
    private Event searchedEvent = new Event();

    @PostConstruct
    public void postConstruct() {
        clearSelectedAthlete();
        clearSearchAthlete();
        updateList();
    }

    public void saveEvent() {
        if (selectedEvent.isValid()) {
            dao.saveEvent(selectedEvent);
            clearSelectedAthlete();
            updateList();
            MessagesUtil.message("Evento salvo com sucesso", FacesMessage.SEVERITY_INFO);
        } else {
            MessagesUtil.message("Não foi possível salvar o evento.",
                    FacesMessage.SEVERITY_ERROR);
        }

    }

    public void deleteEvent(Event event) {
        event.inactivate();
        dao.update(event);
        clearSelectedAthlete();
        updateList();
    }


    public void editEvent(Event event) {
        selectedEvent = event;

    }

    public void searchEvent() {

        boolean hasParam = false;

        if (StringUtils.isNotBlank(searchedEvent.getDescription())) {
            hasParam = true;
            eventList = eventList.stream().filter(
                    event -> event.getDescription().contains(searchedEvent.getDescription())).collect
                    (Collectors.toList());
        }

        if (StringUtils.isNotBlank(searchedEvent.getLocation())) {
            hasParam = true;
            eventList =
                    eventList.stream().filter(
                            event -> event.getLocation().equals(searchedEvent.getLocation())).collect(
                            Collectors.toList());
        }

        if(!hasParam) {
            updateList();
        }

    }

    private void updateList() {
        eventList = new ArrayList<>();
        eventList = dao.getAll();
    }

    private void clearSelectedAthlete() {
        selectedEvent = new Event();
    }

    private void clearSearchAthlete() {
        searchedEvent = new Event();
    }

}
