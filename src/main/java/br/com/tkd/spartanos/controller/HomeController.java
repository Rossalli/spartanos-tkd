package br.com.tkd.spartanos.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import br.com.tkd.spartanos.model.dao.AthleteDAO;
import br.com.tkd.spartanos.model.dao.EventDAO;
import br.com.tkd.spartanos.model.entity.Athlete;
import br.com.tkd.spartanos.model.entity.Event;
import lombok.Getter;

@ManagedBean
public class HomeController {


    private final AthleteDAO athleteDAO = new AthleteDAO();
    private final EventDAO eventDAO = new EventDAO();

    @Getter
    private List<Athlete> birthdaysList = new ArrayList<>();

    @Getter
    private List<Event> eventList = new ArrayList<>();


    @PostConstruct
    public void postConstruct() {
        birthdaysList = getBirthDays();
        eventList = getEvents();
    }

    private List<Athlete> getBirthDays() {
        return athleteDAO.getBirthdays();

    }

    private List<Event> getEvents() {
        return eventDAO.getMonthlyEvents();
    }

}
