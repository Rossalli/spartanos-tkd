package br.com.tkd.spartanos.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;

import org.apache.commons.lang3.StringUtils;

import br.com.tkd.spartanos.model.constants.Gender;
import br.com.tkd.spartanos.model.constants.Graduation;
import br.com.tkd.spartanos.model.dao.AthleteDAO;
import br.com.tkd.spartanos.model.entity.Athlete;
import br.com.tkd.spartanos.util.MessagesUtil;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
public class AthleteController {

    private final AthleteDAO dao = new AthleteDAO();

    @Getter
    @Setter
    private List<Athlete> athleteList = new ArrayList<>();

    @Getter
    @Setter
    private Athlete selectedAthlete = new Athlete();

    @Getter
    @Setter
    private Athlete searchedAthlete = new Athlete();

    @PostConstruct
    public void postConstruct() {
        clearSelectedAthlete();
        clearSearchAthlete();
        updateList();
    }

    public void saveAthlete() {
        if (selectedAthlete.isValid()) {
            dao.saveAthlete(selectedAthlete);
            clearSelectedAthlete();
            updateList();
            MessagesUtil.message("Atleta salvo com sucesso", FacesMessage.SEVERITY_INFO);
        } else {
            MessagesUtil.message("Não foi possível salvar o atleta. Preencha os campos corretamente.",
                    FacesMessage.SEVERITY_ERROR);
        }

    }

    public void deleteAthlete(Athlete athlete) {
        athlete.inactivate();
        dao.update(athlete);
        clearSelectedAthlete();
        updateList();
    }


    public void editAtlete(Athlete athlete) {
        selectedAthlete = athlete;

    }

    public void searchAthlete() {

        boolean hasParam = false;

        if (StringUtils.isNotBlank(searchedAthlete.getName())) {
            hasParam = true;
            athleteList = athleteList.stream().filter(
                    athlete -> athlete.getName().contains(searchedAthlete.getName())).collect
                    (Collectors.toList());
        }

        if (searchedAthlete.getGraduation() != null) {
            hasParam = true;
            athleteList =
                    athleteList.stream().filter(
                            athlete -> athlete.getGraduation().equals(searchedAthlete.getGraduation())).collect(
                            Collectors.toList());
        }

        if (searchedAthlete.getGender() != null) {
            hasParam = true;
            athleteList =
                    athleteList.stream().filter(
                            athlete -> athlete.getGender().equals(searchedAthlete.getGender())).collect(
                            Collectors.toList());
        }

        if(!hasParam) {
            updateList();
        }


    }

    public List<Graduation> getGraduations() {
        return Arrays.asList(Graduation.values());
    }

    public List<Gender> getGenders() {
        return Arrays.asList(Gender.values());
    }

    private void updateList() {
        athleteList = new ArrayList<>();
        athleteList = dao.getAll();
    }

    private void clearSelectedAthlete() {
        selectedAthlete = new Athlete();
    }

    private void clearSearchAthlete() {
        searchedAthlete = new Athlete();
    }

}
