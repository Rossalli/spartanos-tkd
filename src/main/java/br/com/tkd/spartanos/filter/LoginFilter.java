package br.com.tkd.spartanos.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.tkd.spartanos.controller.UserController;

@WebFilter(urlPatterns = "/spartanos/*")
public class LoginFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Object sessionAttribute = ((HttpServletRequest) request).getSession().getAttribute("userController");
        if (sessionAttribute != null) {
            UserController userController = (UserController) sessionAttribute;
            if (!userController.isLogged()) {
                loginRedirect(request, response);
            }
        } else {
            loginRedirect(request, response);
        }

        chain.doFilter(request, response);
    }

    private void loginRedirect(ServletRequest request, ServletResponse response) throws IOException {
        String contextPath = ((HttpServletRequest) request).getContextPath();
        ((HttpServletResponse) response).sendRedirect(contextPath + "/");
    }

    @Override
    public void destroy() {

    }
}
