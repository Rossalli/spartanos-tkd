package br.com.tkd.spartanos.converter;

import java.time.Month;
import java.util.Arrays;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;


@FacesConverter("monthConverter")
public class MonthConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext contet, UIComponent component, String value) {
        if (value.equals(null) || value.equals("null") || value.equals(""))
            return null;
        try {
            return Arrays.stream(Month.values()).filter(month -> month.toString().equals(value))
                    .findFirst().get();

        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext contet, UIComponent component, Object value) {
        if (value == null || value.equals(""))
            return null;
        return value.toString();
    }


}
