package br.com.tkd.spartanos.exception;

import java.util.Iterator;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

public class CustomExceptionHandler extends ExceptionHandlerWrapper {
    private ExceptionHandler wrapped;

    final FacesContext facesContext = FacesContext.getCurrentInstance();

    final Map requestMap = facesContext.getExternalContext().getRequestMap();

    final NavigationHandler navigationHandler = facesContext.getApplication().getNavigationHandler();

    CustomExceptionHandler(ExceptionHandler exception) {
        this.wrapped = exception;
    }

    @Override
    public ExceptionHandler getWrapped() {
        return wrapped;
    }

    @Override
    public void handle() throws FacesException {

        final Iterator iterator = getUnhandledExceptionQueuedEvents().iterator();
        while (iterator.hasNext()) {
            ExceptionQueuedEvent event = (ExceptionQueuedEvent) iterator.next();
            ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();

            Throwable exception = context.getException();

            try {
                exception.printStackTrace();

                requestMap.put("exceptionMessage", exception.getMessage());

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage
                        (FacesMessage.SEVERITY_INFO,
                                "Por favor, tente novamente ou entre em contato  com um dos nossos administradores.",
                                ""));

                navigationHandler.handleNavigation(facesContext, null, "/erro.xhtml");

                // Renderiza a pagina de erro e exibe as mensagens
                facesContext.renderResponse();
            } finally {
                iterator.remove();
            }
        }

        getWrapped().handle();
    }

    public static class CustomExceptionHandlerFactory extends ExceptionHandlerFactory {
        private ExceptionHandlerFactory parent;

        public CustomExceptionHandlerFactory(ExceptionHandlerFactory parent) {
            this.parent = parent;
        }

        @Override
        public ExceptionHandler getExceptionHandler() {
            ExceptionHandler handler = new CustomExceptionHandler(parent.getExceptionHandler());
            return handler;
        }

    }
}
