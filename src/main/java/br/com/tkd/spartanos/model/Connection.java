package br.com.tkd.spartanos.model;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class Connection {
    private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("fatec");

    public static EntityManager getEntityManager() {
        return factory.createEntityManager();
    }
}
