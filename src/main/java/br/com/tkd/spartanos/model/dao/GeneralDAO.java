package br.com.tkd.spartanos.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.tkd.spartanos.model.Connection;

public abstract class GeneralDAO<T> {

    public T save(T entity) {
        EntityManager em = Connection.getEntityManager();
        em.getTransaction().begin();
        em.persist(entity);
        em.getTransaction().commit();
        em.close();
        return entity;
    }


    public T update(T entity) {
        EntityManager em = Connection.getEntityManager();
        em.getTransaction().begin();
        em.merge(entity);
        em.getTransaction().commit();
        em.close();
        return entity;
    }

    public T getOne(String query, HashMap<String, Object> params) {
        EntityManager em = Connection.getEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery(
                query);
        params.forEach((k,v) -> q.setParameter(k, v));
        try {
            T singleResult = (T) q.getSingleResult();
            em.getTransaction().commit();
            em.close();
            return singleResult;
        } catch (NoResultException e) {
            em.getTransaction().commit();
            em.close();
            return null;
        }
    }

    public List<T> getList(String query) {
        List<T> result = new ArrayList<>();
        EntityManager em = Connection.getEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery(
                query);
        result.addAll(q.getResultList());
        em.getTransaction().commit();
        em.close();
        return result;
    }
}
