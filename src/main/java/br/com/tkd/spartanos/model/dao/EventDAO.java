package br.com.tkd.spartanos.model.dao;

import java.time.LocalDateTime;
import java.util.List;

import br.com.tkd.spartanos.model.entity.Event;

public class EventDAO extends GeneralDAO<Event> {

    public void saveEvent(Event event) {
        if (event.getId() == null) {
            save(event);
        } else {
            update(event);
        }
    }

    public List<Event> getAll() {
        String query = "SELECT a FROM Event a WHERE active = true";
        return getList(query);
    }

    public List<Event> getMonthlyEvents() {
        int month = LocalDateTime.now().getMonth().getValue();
        String query = "SELECT a FROM Event a WHERE active = true AND EXTRACT(MONTH FROM date) = " + month;
        return getList(query);
    }
}
