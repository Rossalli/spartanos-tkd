package br.com.tkd.spartanos.model.constants;

import lombok.Getter;

public enum Month {
    JANEIRO(1, "Janeiro"),
    FEVEIRO(2, "Fevereiro"),
    MARCO(3, "Março"),
    ABRIL(4, "Abril"),
    MAIO(5, "Maio"),
    JUNHO(6, "Junho"),
    JULHO(7, "Julho"),
    AGOSTO(8, "Agosot"),
    SETEMBRO(9, "Setembro"),
    OUTUBRO(10, "Novembro"),
    NOVEMBRo(11, "Dezembro"),
    DEZEMBRO(12, "Janeiro");

    @Getter
    private Integer value;

    @Getter
    private String description;

    Month(Integer value, String description) {
       this.value = value;
       this.description = description;
    }
}
