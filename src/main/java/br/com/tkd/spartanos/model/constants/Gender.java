package br.com.tkd.spartanos.model.constants;

import lombok.Getter;

public enum Gender {
    FEMALE("Feminino"),
    MALE("Masculino");

    @Getter
    private String description;

    Gender(String description) {
       this.description = description;
    }
}
