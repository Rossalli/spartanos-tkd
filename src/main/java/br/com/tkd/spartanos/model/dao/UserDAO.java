package br.com.tkd.spartanos.model.dao;

import java.util.HashMap;

import br.com.tkd.spartanos.model.entity.User;

public class UserDAO extends GeneralDAO<User> {

    public User saveUser(User user) {
        return save(user);
    }

    public User login(String login, String password) {
        String query = "SELECT u FROM User u WHERE u.login = :login AND u.password = :password AND u.active = true";
        HashMap<String, Object> params = new HashMap<>();
        params.put("login", login);
        params.put("password", password);
        return getOne(query, params);
    }


    public User getByLogin(String login) {
        String query = "SELECT u FROM User u WHERE u.login = :login AND u.active = true";
        HashMap<String, Object> params = new HashMap<>();
        params.put("login", login);
        return getOne(query, params);
    }
}
