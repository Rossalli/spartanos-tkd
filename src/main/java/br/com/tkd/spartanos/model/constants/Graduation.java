package br.com.tkd.spartanos.model.constants;

import lombok.Getter;

public enum Graduation {
    WHITE("Branca"),
    YELLOW("Amarela"),
    YELLOW_GREEN("Amarela ponta Verde"),
    GREEN("Verde"),
    GREEN_BLUE("Verde ponta aAul"),
    BLUE("Azul"),
    BLUE_RED("Azul ponta Vermelha"),
    RED("Vermelha"),
    RED_BLACK("Vermelha ponta Preta"),
    BLACK("Preta");

    @Getter
    private String description;

    Graduation(String description) {
       this.description = description;
    }
}
