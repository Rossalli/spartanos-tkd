package br.com.tkd.spartanos.model.dao;

import java.time.LocalDateTime;
import java.util.List;

import br.com.tkd.spartanos.model.entity.Athlete;

public class AthleteDAO extends GeneralDAO<Athlete> {

    public void saveAthlete(Athlete athlete) {
        if (athlete.getId() == null) {
            save(athlete);
        } else {
            update(athlete);
        }
    }

    public List<Athlete> getAll() {
        String query = "SELECT a FROM Athlete a WHERE active = true";
        return getList(query);
    }

    public List<Athlete> getBirthdays() {
        int month = LocalDateTime.now().getMonth().getValue();
        String query = "SELECT a FROM Athlete a WHERE active = true AND EXTRACT(MONTH FROM birthdate) = " + month;
        return getList(query);
    }
}
