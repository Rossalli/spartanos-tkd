package br.com.tkd.spartanos.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.tkd.spartanos.model.constants.Gender;
import br.com.tkd.spartanos.model.constants.Graduation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "`athlete`")
public class Athlete implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(unique = true)
    private String nickname;

    @Enumerated(EnumType.STRING)
    private Graduation graduation;

    private BigDecimal weight;

    private Long height;

    private String phone;

    private String address;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "emergency_contact")
    private String emergencyContact;

    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "started_at")
    private Date startedAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_at")
    private Date updatedAt;

    private Boolean active;

    private Boolean coach;

    @PrePersist
    private void onCreate() {
        createdAt = new Date();
        active = true;
    }

    @PreUpdate
    private void oUpdate() {
        updatedAt = new Date();
    }

    public Long getAge() {
        LocalDateTime localDateTime = birthDate.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
        return ChronoUnit.YEARS.between(localDateTime, LocalDateTime.now());
    }


    public String getFormatBirthdate() {
        return new SimpleDateFormat("dd/MM").format(birthDate);
    }

    public void inactivate() {
        active = false;
    }

    public boolean isValid() {
        if (name == null) return false;
        if (nickname == null) return false;
        if (graduation == null) return false;
        if (weight == null) return false;
        if (height == null) return false;
        if (phone == null) return false;
        if (address == null) return false;
        if (gender == null) return false;
        if (emergencyContact == null) return false;
        if (birthDate == null) return false;
        if (startedAt == null) return false;
        return true;
    }

}