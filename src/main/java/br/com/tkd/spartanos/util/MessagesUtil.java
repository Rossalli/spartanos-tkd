package br.com.tkd.spartanos.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class MessagesUtil {

    public  static void message(String message, FacesMessage.Severity severity) {
        FacesMessage msg = new FacesMessage(message, "");
        msg.setSeverity(severity);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
