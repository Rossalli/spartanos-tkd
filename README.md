## Pré-Requisitos:

Para desenvolver neste projeto e rodar local são necessários os seguintes softwares instalados e configurados:

 * JDK 8
 * Maven
 * Postgres 9.5
 * Lombok Plugin
 * Tomcart 7

## Banco de Dados

 * database: fatec
 * usuario: postgres
 * senha: postgres

 
## Rodando o projeto

```
mvn clean install tomcat7:run
```

A aplicação estará disponível em: http://localhost:8080
